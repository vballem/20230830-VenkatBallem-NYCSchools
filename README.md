# 20230828-VenkatBallem-NYCSchools

## Development Environment

Windows 10 Pro
Android Studio Flamingo | 2022.2.1 Patch 2
JDK: Jetbrains Runtime 17
Phones:
Samsung Galaxy S9 Plus, Android 10 - Real Device
Pixel 6A Emulator - API 33

## KeyNotes

Below are the areas to be further improved AFAIK.

1. Repository class is created for separation of concerns. However at present it does less work.
   As the rest api does not of paging from server side, I could not opt for it. using flows is
   recommended in this layer.
2. Repository must talk to one or more data sources, either it could be a remote rest api data
   source, or database.
   RemoteDataSource is created to pull data from server api by using Retrofit. Now a kotlin offers
   Ktor-Client which is also good fit here.
3. Handling Nitty-Gritty UI details. I am aware of that the there is lot of scope to improve UI,
   mainly theming.
4. Using Kotlin Delegates instead of base class approach for Fragments and ViewModels.
5. The SatScore Screen UI XML has duplicate code of CardView( 3 times) which can be implemented in a
   Custom CardView or can be redesigned in another view to reduce view hierarchy.
6. UI Tests highly recommended, However I would need some more time. but no time :)
7. I have not used Mockito or Mockk. As every class is dependent on an interface, So creating Fake Impl
   in test package is highly recommended instead of using mocks as it kills time.

# Thank You
   