package com.virtusa.app.ui.score

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.virtusa.app.FakeSchoolDataRepository
import com.virtusa.app.MainDispatcherRule
import com.virtusa.app.StatusType
import com.virtusa.app.connectivity.FakeConnectivityMonitor
import com.virtusa.app.getOrAwaitValue
import com.virtusa.app.network.model.SATScore
import junit.framework.TestCase
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SATScoreViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    private lateinit var subject: SATScoreViewModel
    private val fakeRepository = FakeSchoolDataRepository()
    private val fakeConnectivityMonitor = FakeConnectivityMonitor()

    @Before
    fun setUp() {
        subject = SATScoreViewModel(
            fakeRepository,
            fakeConnectivityMonitor,
            dispatcherRule.testDispatcher
        )
    }

    @Test
    fun notifyLoadingStateWhenStarted() = runTest {
        assertEquals(SatScoreBoardUiState.Loading, subject.uiState.value)
    }

    @Test
    fun notifySuccessState() = runTest {
        fakeRepository.givenScoresMap.putAll(
            mutableMapOf(
                "1000" to SATScore(dbn = "1000"),
                "1001" to SATScore(dbn = "1001", writeAvgScore = "99")
            )
        )
        fakeRepository.givenStatus = StatusType.SUCCESS

        subject.get("1001")

        val resultUiSate = subject.uiState.getOrAwaitValue()

        assertEquals("99", (resultUiSate as SatScoreBoardUiState.Success).satScore.writeAvgScore)
    }

    @Test
    fun notifySuccessStateIfScoreIsEmptyAlso() = runTest {
        fakeRepository.givenScoresMap.putAll(
            mutableMapOf(
                "1000" to SATScore(dbn = "1000")
            )
        )
        fakeRepository.givenStatus = StatusType.REPORT_EMPTY_DATASET

        subject.get("1001")

        val resultUiSate = subject.uiState.getOrAwaitValue()

        assertEquals("", (resultUiSate as SatScoreBoardUiState.Success).satScore.writeAvgScore)
    }

    @Test
    fun notifyFailureState() = runTest {
        fakeRepository.givenScoresMap.putAll(
            mutableMapOf(
                "1000" to SATScore(dbn = "1000"),
            )
        )
        fakeRepository.givenStatus = StatusType.REPORT_GENERIC_ERROR

        subject.get("1000")

        val resultUiSate = subject.uiState.getOrAwaitValue()

        assertEquals(500, (resultUiSate as SatScoreBoardUiState.Error).code)
    }

    @Test
    fun notifyOnInternetOffline() {
        fakeConnectivityMonitor.setConnected(false)

        subject.start()

        TestCase.assertFalse(subject.connectivityState.getOrAwaitValue())
    }

    @Test
    fun notifyOnInternetOnline() = runTest {
        fakeConnectivityMonitor.setConnected(true)

        subject.start()

        TestCase.assertTrue(subject.connectivityState.getOrAwaitValue())
    }

    @After
    fun tearDown() {
        fakeRepository.givenStatus = StatusType.SUCCESS
        fakeRepository.givenScoresMap.clear()
    }

}