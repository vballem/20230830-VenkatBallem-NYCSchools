package com.virtusa.app.ui.dashboard

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.virtusa.app.FakeSchoolDataRepository
import com.virtusa.app.MainDispatcherRule
import com.virtusa.app.StatusType
import com.virtusa.app.connectivity.FakeConnectivityMonitor
import com.virtusa.app.getOrAwaitValue
import com.virtusa.app.network.model.School
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DashboardViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    private lateinit var subject: DashboardViewModel
    private val fakeSchoolDataRepository = FakeSchoolDataRepository()
    private val fakeConnectivityMonitor = FakeConnectivityMonitor()

    @Before
    fun setUp() {
        subject = DashboardViewModel(
            fakeSchoolDataRepository,
            fakeConnectivityMonitor,
            dispatcherRule.testDispatcher
        )
    }

    @Test
    fun notifyLoadingStateWhenStarted() = runTest {
        assertEquals(DashBoardUiState.Loading, subject.uiState.value)
    }

    @Test
    fun notifySuccessState() = runTest {
        fakeSchoolDataRepository.givenSchoolsData.addAll(
            mutableListOf(
                School(dbn = "1001", name = "ABC School")
            )
        )

        fakeSchoolDataRepository.givenStatus = StatusType.SUCCESS

        subject.load()

        val resultUiSate = subject.uiState.getOrAwaitValue()

        assertEquals("ABC School", (resultUiSate as DashBoardUiState.Success).schoolsData[0].name)
    }

    @Test
    fun notifyFailureState() = runTest {
        fakeSchoolDataRepository.givenSchoolsData.addAll(
            mutableListOf(
                School(dbn = "1000", name = "ABC School")
            )
        )
        fakeSchoolDataRepository.givenStatus = StatusType.REPORT_GENERIC_ERROR

        subject.load()

        val resultUiSate = subject.uiState.getOrAwaitValue()
        assertEquals(500, (resultUiSate as DashBoardUiState.Error).code)
    }

    @Test
    fun notifyOnInternetOffline() {
        fakeConnectivityMonitor.setConnected(false)

        subject.start()

        assertFalse(subject.connectivityState.getOrAwaitValue())
    }

    @Test
    fun notifyOnInternetOnline() = runTest {
        fakeConnectivityMonitor.setConnected(true)

        subject.start()

        assertTrue(subject.connectivityState.getOrAwaitValue())
    }

    @After
    fun tearDown() {
        fakeSchoolDataRepository.givenScoresMap.clear()
    }

}