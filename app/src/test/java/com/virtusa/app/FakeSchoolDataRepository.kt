package com.virtusa.app

import com.virtusa.app.network.NetworkResponse
import com.virtusa.app.network.RemoteDataSource
import com.virtusa.app.network.model.SATScore
import com.virtusa.app.network.model.School
import com.virtusa.app.repository.SchoolRepository

sealed class StatusType {
    object UNKNOWN : StatusType()
    object SUCCESS : StatusType()
    object REPORT_GENERIC_ERROR : StatusType()
    object REPORT_EMPTY_DATASET : StatusType()
}

class FakeSchoolDataRepository : SchoolRepository {

    val givenSchoolsData = mutableListOf<School>()
    val givenScoresMap = mutableMapOf<String, SATScore>()
    var givenStatus: StatusType = StatusType.UNKNOWN

    override suspend fun getAllSchools(): NetworkResponse<List<School>> {
        return when (givenStatus) {
            StatusType.REPORT_EMPTY_DATASET -> NetworkResponse.Error(
                RemoteDataSource.EMPTY_SCORE, "Not Found"
            )

            StatusType.REPORT_GENERIC_ERROR -> NetworkResponse.Error(500, "Internal Server Error")

            StatusType.SUCCESS -> NetworkResponse.Success(givenSchoolsData)
            else -> {
                throw IllegalAccessException("Something with how u given the test data")
            }
        }
    }

    override suspend fun getSATResult(dbn: String): NetworkResponse<SATScore> {
        return when (givenStatus) {
            StatusType.REPORT_EMPTY_DATASET -> NetworkResponse.Error(
                RemoteDataSource.EMPTY_SCORE, "Not Found"
            )

            StatusType.REPORT_GENERIC_ERROR -> NetworkResponse.Error(500, "Internal Server Error")

            StatusType.SUCCESS -> NetworkResponse.Success(givenScoresMap[dbn]!!)
            else -> {
                throw IllegalAccessException("Something with how u given the test data")
            }
        }
    }
}
