package com.virtusa.app.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.test.runTest
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import org.junit.Test
import retrofit2.Retrofit

class RemoteDataSourceImplTest : MockServerTest() {

    private lateinit var subject: RemoteDataSource

    private val json = Json {
        ignoreUnknownKeys = true
    }

    override fun onSetUp() {
        subject = RemoteDataSourceImpl(
            Retrofit.Builder().baseUrl(mockWebServer.url("")).addConverterFactory(
                json.asConverterFactory("application/json".toMediaType())
            ).build().create(NYCHighSchoolApi::class.java)
        )
    }

    @Test
    fun getListOfSchoolsData() = runTest {
        enqueueResponse("all_schools_valid_data.json")

        val schools = subject.getAllSchools()

        assertTrue(schools is NetworkResponse.Success)
        assertEquals(3, (schools as NetworkResponse.Success).data.size)
    }

    @Test
    fun getEmptyListOfSchoolsData() = runTest {
        enqueueResponse("all_schools_data_empty.json")

        val schools = subject.getAllSchools()

        assertTrue(schools is NetworkResponse.Success)
        assertEquals(0, (schools as NetworkResponse.Success).data.size)
    }

    @Test
    fun getInternalServerErrorWhileFetchingSchoolsData() = runTest {
        enqueueResponse("all_schools_valid_data.json", 404)

        val schools = subject.getAllSchools()

        assertTrue(schools is NetworkResponse.Error)
    }

    @Test
    fun throwExceptionWhileFetchingListOfSchoolsData() = runTest {
        enqueueResponse("all_schools_corrupted_data.json")

        val schools = subject.getAllSchools()

        assertTrue(schools is NetworkResponse.Error)
    }

    @Test
    fun getSatScoreForGivenSchool() = runTest {
        enqueueResponse("sat_score_valid_response.json")

        val satScore = subject.getSATResultById("01M448") as NetworkResponse.Success

        assertEquals("01M448", satScore.data.dbn)
        assertEquals("383", satScore.data.readingAvgScore)
        assertEquals("423", satScore.data.mathAvgScore)
        assertEquals("366", satScore.data.writeAvgScore)
    }

    @Test
    fun getInternalServerErrorWhileFetchingSatScore() = runTest {
        enqueueResponse("sat_score_valid_response.json", 404)

        val schools = subject.getSATResultById("01M448")

        assertTrue(schools is NetworkResponse.Error)
    }

    @Test
    fun throwExceptionWhileFetchingSatScore() = runTest {
        enqueueResponse("sat_score_invalid_response.json")

        val schools = subject.getSATResultById("01M448")

        assertTrue(schools is NetworkResponse.Error)
    }

}