package com.virtusa.app.network

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before

import java.nio.charset.StandardCharsets.UTF_8

abstract class MockServerTest {
    protected lateinit var mockWebServer: MockWebServer

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        mockWebServer.url("https://data.cityofnewyork.us")
        onSetUp()
    }

    fun enqueueResponse(jsonFileName: String, responseCode: Int = 200) {
        val inputStream = javaClass.classLoader?.getResourceAsStream(jsonFileName)
        val source = inputStream?.let { inputStream.source().buffer() }!!

        val response = MockResponse()
            .setBody(source.readString(UTF_8))
            .setResponseCode(responseCode)
            .addHeader("Content-Type", "application/json")

        mockWebServer.enqueue(response)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
        onTearDown()
    }

    abstract fun onSetUp()

    private fun onTearDown() {
        //Default does nothing
    }
}