package com.virtusa.app.repository

import com.virtusa.app.network.NetworkResponse
import com.virtusa.app.network.model.SATScore
import com.virtusa.app.network.model.School

/**
 * The Purpose of this class at this present context is useless.
 * However, to follow the seperation-of-concerns principle, its recommended to have a repository pattern
 * The main power will come if the returns Flows or combine database layer or multiple data layers
 *
 * As per the given Rest API, it does not support pagination, Hence using Flow/LiveData does not make sense
 */
interface SchoolRepository {
    suspend fun getAllSchools(): NetworkResponse<List<School>>
    suspend fun getSATResult(dbn: String): NetworkResponse<SATScore>
}
