package com.virtusa.app.repository

import javax.inject.Inject
import javax.inject.Singleton
import com.virtusa.app.network.NetworkResponse
import com.virtusa.app.network.RemoteDataSource
import com.virtusa.app.network.model.SATScore
import com.virtusa.app.network.model.School

/**
 * The Purpose of this class at present context is useless as it does blind pure delegation.
 * However, to follow the 'separation-of-concerns' principle, its recommended to have a repository pattern.
 * The main power will come if the returns flows or combine database layer or multiple data sources.
 *
 * As per the given Rest API, it does not support pagination, Hence using Flow/LiveData does not make sense.
 * Hence, did not add unit tests as well.
 *
 */

@Singleton
class SchoolRepositoryImpl @Inject constructor(
    private val dataSource: RemoteDataSource
) : SchoolRepository {

    private var scoresMap = mutableMapOf<String, NetworkResponse<SATScore>>()
    private var allSchools: NetworkResponse<List<School>>? = null
    override suspend fun getAllSchools(): NetworkResponse<List<School>> {
        return allSchools ?: kotlin.run {
            allSchools = dataSource.getAllSchools()
            allSchools!!
        }
    }

    override suspend fun getSATResult(dbn: String): NetworkResponse<SATScore> {
        return scoresMap.getOrElse(dbn) {
            val result = dataSource.getSATResultById(dbn)
            scoresMap[dbn] = result
            result
        }
    }
}
