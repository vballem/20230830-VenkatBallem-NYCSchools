package com.virtusa.app.connectivity.di

import com.virtusa.app.connectivity.ConnectivityMonitor
import com.virtusa.app.connectivity.ConnectivityMonitorImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface ConnectivityModule {
    @Binds
    fun bindsNetworkMonitor(
        networkMonitor: ConnectivityMonitorImpl,
    ): ConnectivityMonitor
}