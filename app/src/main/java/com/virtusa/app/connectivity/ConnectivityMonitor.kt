package com.virtusa.app.connectivity

import kotlinx.coroutines.flow.Flow

interface ConnectivityMonitor {

    companion object {
        const val INTERNET_OFFLINE_ERROR = 300
    }

    val isOnline: Flow<Boolean>
}
