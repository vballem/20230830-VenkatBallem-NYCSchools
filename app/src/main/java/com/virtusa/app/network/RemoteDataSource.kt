package com.virtusa.app.network

import com.virtusa.app.network.model.SATScore
import com.virtusa.app.network.model.School

interface RemoteDataSource {
    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us"
        const val GENERIC_ERROR_CODE = 100
        const val EMPTY_SCORE = 101
    }

    suspend fun getAllSchools(): NetworkResponse<List<School>>
    suspend fun getSATResultById(dbn: String): NetworkResponse<SATScore>
}