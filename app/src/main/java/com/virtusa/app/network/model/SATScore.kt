package com.virtusa.app.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SATScore(
    val dbn: String = "",
    @SerialName("school_name")
    val schoolName: String = "",
    @SerialName("num_of_sat_test_takers")
    val testTakersCount: String = "",
    @SerialName("sat_critical_reading_avg_score")
    val readingAvgScore: String = "",
    @SerialName("sat_math_avg_score")
    val mathAvgScore: String = "",
    @SerialName("sat_writing_avg_score")
    val writeAvgScore: String = ""
)