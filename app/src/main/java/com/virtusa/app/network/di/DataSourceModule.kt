package com.virtusa.app.network.di

import com.virtusa.app.network.RemoteDataSource
import com.virtusa.app.network.RemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DataSourceModule {

    @Binds
    fun RemoteDataSourceImpl.binds(): RemoteDataSource
}