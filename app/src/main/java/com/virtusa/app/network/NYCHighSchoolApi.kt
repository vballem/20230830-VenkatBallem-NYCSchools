package com.virtusa.app.network

import com.virtusa.app.network.model.SATScore
import com.virtusa.app.network.model.School
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NYCHighSchoolApi {

    @GET(value = "resource/s3k6-pzi2.json")
    suspend fun getAllSchoolsInfo(): Response<List<School>>

    @GET(value = "resource/f9bf-2cp4.json")
    suspend fun getSATResultById(@Query("dbn") id: String): Response<List<SATScore>>
}