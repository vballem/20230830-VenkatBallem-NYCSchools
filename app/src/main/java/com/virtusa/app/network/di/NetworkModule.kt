package com.virtusa.app.network.di

import javax.inject.Singleton
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.virtusa.app.network.NYCHighSchoolApi
import com.virtusa.app.network.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.Call
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun providesRetrofitClientApi(
        converterFactory: Converter.Factory,
        callFactory: Call.Factory
    ): NYCHighSchoolApi {
        return Retrofit.Builder()
            .baseUrl(RemoteDataSource.BASE_URL)
            .addConverterFactory(converterFactory)
            .callFactory(callFactory)
            .build().create(NYCHighSchoolApi::class.java)
    }

    private val json = Json {
        ignoreUnknownKeys = true
    }

    @Provides
    @Singleton
    fun providesNetworkJson(): Converter.Factory =
        json.asConverterFactory("application/json".toMediaType())

    @Provides
    @Singleton
    fun okHttpCallFactory(): Call.Factory = OkHttpClient.Builder()
        .addInterceptor(
            HttpLoggingInterceptor()
                .apply {
                    setLevel(HttpLoggingInterceptor.Level.BODY)
                },
        )
        .build()

}