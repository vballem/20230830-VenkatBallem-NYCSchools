package com.virtusa.app.network

import javax.inject.Inject
import javax.inject.Singleton
import com.virtusa.app.network.RemoteDataSource.Companion.EMPTY_SCORE
import com.virtusa.app.network.RemoteDataSource.Companion.GENERIC_ERROR_CODE
import com.virtusa.app.network.model.SATScore
import com.virtusa.app.network.model.School
import retrofit2.HttpException

@Singleton
class RemoteDataSourceImpl @Inject constructor(
    private val networkApi: NYCHighSchoolApi
) : RemoteDataSource {

    override suspend fun getAllSchools(): NetworkResponse<List<School>> {
        return try {
            val response = networkApi.getAllSchoolsInfo()
            val body = response.body()
            if (response.isSuccessful && body != null) {
                NetworkResponse.Success(body)
            } else {
                NetworkResponse.Error(response.code(), response.message())
            }
        } catch (e: HttpException) {
            NetworkResponse.Error(e.code(), e.message())
        } catch (e: Throwable) {
            NetworkResponse.Error(GENERIC_ERROR_CODE, e.localizedMessage)
        }
    }

    override suspend fun getSATResultById(dbn: String): NetworkResponse<SATScore> {
        return try {
            val response = networkApi.getSATResultById(dbn)
            val body = response.body()
            if (response.isSuccessful && body != null) {
                if (body.isEmpty()) {
                    NetworkResponse.Error(EMPTY_SCORE, "No Data")
                } else {
                    NetworkResponse.Success(body[0])
                }
            } else {
                NetworkResponse.Error(response.code(), response.message())
            }
        } catch (e: HttpException) {
            NetworkResponse.Error(e.code(), e.message())
        } catch (e: Throwable) {
            NetworkResponse.Error(GENERIC_ERROR_CODE, e.localizedMessage)
        }
    }
}
