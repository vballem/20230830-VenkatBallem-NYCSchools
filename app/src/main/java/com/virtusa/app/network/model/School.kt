package com.virtusa.app.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class School(
    val dbn: String = "",
    @SerialName("school_name")
    val name: String = "",
    @SerialName("overview_paragraph")
    val overview: String = "",
    @SerialName("phone_number")
    val phoneNumber: String = "",
    @SerialName("primary_address_line_1")
    val streetName: String = "",
    val city: String = "",
    @SerialName("zip")
    val zip: String = "",
    @SerialName("state_code")
    val stateId: String = "",
    val website: String = ""
)