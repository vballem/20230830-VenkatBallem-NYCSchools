package com.virtusa.app.ui.score

import com.virtusa.app.network.model.SATScore

sealed interface SatScoreBoardUiState {
    object Loading : SatScoreBoardUiState
    class Error(val code: Int, val msg: String?) : SatScoreBoardUiState
    data class Success(val satScore: SATScore) : SatScoreBoardUiState
}