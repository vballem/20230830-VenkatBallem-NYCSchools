package com.virtusa.app.ui.score

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.virtusa.app.R
import com.virtusa.app.databinding.FragmentSatScoreBinding
import com.virtusa.app.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SATScoreFragment :
    BaseFragment<SATScoreViewModel, FragmentSatScoreBinding>(FragmentSatScoreBinding::inflate) {

    private val viewModel: SATScoreViewModel by viewModels()
    private val args: SATScoreFragmentArgs by navArgs()

    override fun bindData() {
        binding.schoolName = args.schoolName
        viewModel.get(args.schoolId)
        viewModel.uiState.observe(viewLifecycleOwner) {
            when (it) {
                is SatScoreBoardUiState.Loading -> {
                    updateUI(true, binding.loadingView, binding.schoolsDataView)
                }

                is SatScoreBoardUiState.Success -> {
                    updateUI(true, binding.schoolsDataView, binding.loadingView)

                    binding.satScore = it.satScore
                    binding.executePendingBindings()
                }

                is SatScoreBoardUiState.Error -> showSnackBar(it.msg) {
                    viewModel.get(args.schoolId)
                }
            }

        }

        viewModel.connectivityState.observe(viewLifecycleOwner) { isOnline ->
            if (isOnline && viewModel.uiState.value !is SatScoreBoardUiState.Success) {
                binding.loadingView.text = getString(R.string.loading_data_please_wait)
                viewModel.get(args.schoolId)
                dismissSnackBar()
            } else if (viewModel.uiState.value !is SatScoreBoardUiState.Success) {
                binding.loadingView.text = getString(R.string.please_turn_on_internet)
            } else {
                Log.d("ScoreFragment", "loaded and internet is available, nothing to do")
            }
        }
    }

    override fun viewModel() = viewModel
}
