package com.virtusa.app.ui.dashboard

import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.virtusa.app.databinding.DashboardItemViewBinding
import com.virtusa.app.network.model.School

class DashboardItemViewHolder(private val binding: DashboardItemViewBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(school: School) {
        binding.school = school
        binding.executePendingBindings()

        binding.root.setOnClickListener {
            binding.root.findNavController()
                .navigate(DashboardFragmentDirections.openSatScore(school.dbn, school.name))
        }
    }

}