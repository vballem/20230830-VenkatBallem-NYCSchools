package com.virtusa.app.ui.score

import javax.inject.Inject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.virtusa.app.connectivity.ConnectivityMonitor
import com.virtusa.app.network.NetworkResponse
import com.virtusa.app.network.RemoteDataSource
import com.virtusa.app.network.model.SATScore
import com.virtusa.app.repository.SchoolRepository
import com.virtusa.app.ui.BaseViewModel
import com.virtusa.app.ui.di.IoDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@HiltViewModel
class SATScoreViewModel @Inject constructor(
    repository: SchoolRepository,
    connectivityMonitor: ConnectivityMonitor,
    @IoDispatcher ioDispatcher: CoroutineDispatcher
) : BaseViewModel(repository, connectivityMonitor, ioDispatcher) {

    private val _uiState = MutableLiveData<SatScoreBoardUiState>(SatScoreBoardUiState.Loading)
    val uiState = _uiState

    fun get(dbn: String) {
        if (!isRequestInProgress) {
            isRequestInProgress = true
            viewModelScope.launch(ioDispatcher) {
                val response = repository.getSATResult(dbn)
                withContext(Dispatchers.Main) {
                    when (response) {
                        is NetworkResponse.Error -> {
                            uiState.value = if (response.code == RemoteDataSource.EMPTY_SCORE) {
                                SatScoreBoardUiState.Success(SATScore())
                            } else {
                                SatScoreBoardUiState.Error(response.code, response.message)
                            }
                        }

                        is NetworkResponse.Success -> uiState.value =
                            SatScoreBoardUiState.Success(response.data)
                    }
                    isRequestInProgress = false
                }
            }
        }
    }
}