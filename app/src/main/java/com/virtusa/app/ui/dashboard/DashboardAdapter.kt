package com.virtusa.app.ui.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.virtusa.app.R
import com.virtusa.app.databinding.DashboardItemViewBinding
import com.virtusa.app.network.model.School

class DashboardAdapter(val schools: MutableList<School>) :
    RecyclerView.Adapter<DashboardItemViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): DashboardItemViewHolder {
        val binder = DataBindingUtil.inflate<DashboardItemViewBinding>(
            LayoutInflater.from(viewGroup.context),
            R.layout.dashboard_item_view, viewGroup, false
        )
        return DashboardItemViewHolder(binder)
    }

    override fun onBindViewHolder(viewHolder: DashboardItemViewHolder, position: Int) {
        viewHolder.bind(schools[position])
    }

    override fun getItemCount() = schools.size

}