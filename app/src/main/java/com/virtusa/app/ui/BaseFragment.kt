package com.virtusa.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.google.android.material.snackbar.Snackbar
import com.virtusa.app.R

abstract class BaseFragment<VM : BaseViewModel, VB : ViewBinding>(
    private val viewInflater: (layoutInflater: LayoutInflater) -> VB
) : Fragment() {

    private var snackBar: Snackbar? = null
    private var _binding: VB? = null
    protected val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        _binding = viewInflater.invoke(inflater)
        bindData()
        viewModel().start()
        return binding.root
    }

    abstract fun viewModel(): VM

    abstract fun bindData()

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun showSnackBar(msg: String?, block: () -> Unit) {
        msg?.let {
            snackBar = Snackbar.make(binding.root, it, Snackbar.LENGTH_INDEFINITE)
            snackBar?.setAction(getString(R.string.retry)) {
                snackBar?.dismiss()
                block()
            }
            snackBar?.show()
        }
    }

    fun updateUI(isLoading: Boolean, visibleView: View, goneView: View) {
        if (isLoading) {
            visibleView.visibility = View.VISIBLE
            goneView.visibility = View.GONE
        } else {
            visibleView.visibility = View.GONE
            goneView.visibility = View.VISIBLE
        }
    }

    fun dismissSnackBar() {
        snackBar?.let {
            if (it.isShownOrQueued)
                it.dismiss()
        }
    }
}
