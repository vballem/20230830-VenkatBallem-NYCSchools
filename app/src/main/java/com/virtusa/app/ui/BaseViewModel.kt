package com.virtusa.app.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.virtusa.app.connectivity.ConnectivityMonitor
import com.virtusa.app.repository.SchoolRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

abstract class BaseViewModel constructor(
    protected val repository: SchoolRepository,
    private val connectivityMonitor: ConnectivityMonitor,
    protected val ioDispatcher: CoroutineDispatcher
) : ViewModel() {

    protected var isRequestInProgress = false
    private val _connectivityState = MutableLiveData<Boolean>()
    val connectivityState = _connectivityState

    fun start() {
        viewModelScope.launch {
            connectivityMonitor.isOnline.collect {
                connectivityState.postValue(it)
            }
        }
    }
}