package com.virtusa.app.ui.dashboard

import com.virtusa.app.network.model.School

sealed interface DashBoardUiState {
    object Loading : DashBoardUiState
    class Error(val code: Int, val msg: String?) : DashBoardUiState
    data class Success(val schoolsData: List<School>) : DashBoardUiState
}