package com.virtusa.app.ui.dashboard

import javax.inject.Inject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.virtusa.app.connectivity.ConnectivityMonitor
import com.virtusa.app.network.NetworkResponse
import com.virtusa.app.repository.SchoolRepository
import com.virtusa.app.ui.BaseViewModel
import com.virtusa.app.ui.di.IoDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

@HiltViewModel
class DashboardViewModel @Inject constructor(
    repository: SchoolRepository,
    connectivityMonitor: ConnectivityMonitor,
    @IoDispatcher ioDispatcher: CoroutineDispatcher
) : BaseViewModel(repository, connectivityMonitor, ioDispatcher) {

    private val _uiState = MutableLiveData<DashBoardUiState>(DashBoardUiState.Loading)
    val uiState = _uiState

    fun load() {
        if (!isRequestInProgress) {
            isRequestInProgress = true
            viewModelScope.launch(ioDispatcher) {

                when (val response = repository.getAllSchools()) {
                    is NetworkResponse.Error -> uiState.postValue(
                        DashBoardUiState.Error(response.code, response.message)
                    )

                    is NetworkResponse.Success -> uiState.postValue(
                        DashBoardUiState.Success(
                            response.data
                        )
                    )
                }
                isRequestInProgress = false
            }
        }
    }
}