package com.virtusa.app.ui

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.virtusa.app.R
import com.virtusa.app.network.model.SATScore
import com.virtusa.app.network.model.School

@BindingAdapter("address")
fun setAddress(view: TextView, school: School) {
    val address = StringBuilder()
    address.append(school.phoneNumber).append("\n").append(school.streetName).append("\n")
        .append(school.city).append(", ").append(school.stateId).append(", ").append(school.zip)
        .append("\n").append(school.website)
    view.text = address
}

@BindingAdapter("score")
fun setSatScore(view: TextView, score: String?) {
    if (score.isNullOrEmpty() || score == "s") {
        view.text = view.resources.getString(R.string.not_available)
    } else {
        view.text = score
    }
}

@BindingAdapter("schoolDesc")
fun setSchoolDesc(view: TextView, score: SATScore?) {
    score?.let {
        if (it.schoolName.isEmpty() || it.mathAvgScore == "s") {
            view.text = view.resources.getString(R.string.record_not_found)
        } else {
            view.text =
                String.format(view.resources.getString(R.string.no_test_takers), it.testTakersCount)
        }
    }
}