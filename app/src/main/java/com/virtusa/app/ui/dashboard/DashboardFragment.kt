package com.virtusa.app.ui.dashboard

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
import com.virtusa.app.R
import com.virtusa.app.databinding.FragmentDashboardBinding
import com.virtusa.app.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class DashboardFragment :
    BaseFragment<DashboardViewModel, FragmentDashboardBinding>(FragmentDashboardBinding::inflate) {

    private val viewModel: DashboardViewModel by viewModels()
    private lateinit var mLayoutManager: LinearLayoutManager
    override fun bindData() {
        val adapter = DashboardAdapter(mutableListOf())
        mLayoutManager = LinearLayoutManager(requireContext())
        binding.schoolsListView.layoutManager = mLayoutManager
        binding.schoolsListView.adapter = adapter
        binding.schoolsListView.adapter?.stateRestorationPolicy = PREVENT_WHEN_EMPTY

        viewModel.uiState.observe(viewLifecycleOwner) {
            when (it) {
                is DashBoardUiState.Loading -> {
                    updateUI(true, binding.loadingView, binding.schoolsListView)
                }

                is DashBoardUiState.Success -> {
                    updateUI(false, binding.loadingView, binding.schoolsListView)

                    adapter.schools.addAll(it.schoolsData)
                    adapter.notifyItemRangeInserted(0, it.schoolsData.size - 1)
                }

                is DashBoardUiState.Error -> {
                    //Lot of room to improve this error handling flow depends on the error code
                    //Can guide user to enable internet or retry...
                    showSnackBar(it.msg) {
                        viewModel.load()
                    }
                }
            }
        }

        viewModel.connectivityState.observe(viewLifecycleOwner) { isOnline ->
            if (isOnline && viewModel.uiState.value !is DashBoardUiState.Success) {
                binding.loadingView.text = getString(R.string.loading_data_please_wait)
                viewModel.load()
                dismissSnackBar()
            } else if (viewModel.uiState.value !is DashBoardUiState.Success) {
                binding.loadingView.text = getString(R.string.please_turn_on_internet)
            } else {
                Log.d("DashboardFragment", "loaded and internet is available, nothing to do")
            }
        }
        viewModel.load()
    }

    override fun viewModel() = viewModel

}

